/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

/**
 *
 * @author Satit Wapeetao
 */
public abstract class Shape {
    private String shapeName;
    public Shape(String shapeName){
        
    } 

    public String getShapeName() {
        return shapeName;
    }
    public abstract double calArea();
    public abstract double calperimeter();
    
}
