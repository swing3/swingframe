/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

/**
 *
 * @author Satit Wapeetao
 */
public class Triangle extends Shape{
    private double Base;
    private double Height;
    public Triangle(double Height,double Base) {
        super("Triangle");
        this.Height=Height;
        this.Base=Base;
    }

    public double getBase() {
        return Base;
    }

    public void setBase(double Base) {
        this.Base = Base;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }
    
    @Override
    public double calArea() {
        return 0.5*Height*Base;
    }

    @Override
    public double calperimeter() {
        return Height+Height+Base;
    }
    
}
