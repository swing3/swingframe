/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

/**
 *
 * @author Satit Wapeetao
 */
public class Square extends Shape {

    private double side;

    public Square(double side) {
        super("Square");
        this.side = side;
    }

    public double getS() {
        return side;
    }

    public void setS(double side) {
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public double calperimeter() {
        return side * 4;
    }

}
