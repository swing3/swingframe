/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Satit Wapeetao
 */
public class CircleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("radius:",JLabel.TRAILING);
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.LIGHT_GRAY);
        lblRadius.setOpaque(true);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50,20);
        txtRadius.setLocation(60, 5);
        
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(120, 5);
        
        
        final JLabel lblResult = new JLabel("Circle radius= ? area = ? perimeter= ? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setBackground(Color.magenta);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 50);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strRadius = txtRadius.getText();
                double radius = Double.parseDouble(strRadius);
                Circle circle = new Circle(radius);
                lblResult.setText("Circle radius = "+circle.getRadius()
                        +" area = "+String.format("%.2f", circle.calArea())
                        +" perimeter = "+String.format("%.2f",circle.calperimeter()));
            }catch(Exception ex){
                JOptionPane.showMessageDialog(frame,"Error:Pleasr input number",
                        "Error", JOptionPane.ERROR_MESSAGE);
                txtRadius.setText("");
                txtRadius.requestFocus();
            }           
            }    
            
        });
        
        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtRadius);
        frame.add(lblRadius);
        frame.setVisible(true);
    }
}
