/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Satit Wapeetao
 */
public class RectangleFrame {
public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWeightLong = new JLabel("Weight&Long:", JLabel.TRAILING);
        lblWeightLong.setSize(90, 20);
        lblWeightLong.setLocation(5, 5);
        lblWeightLong.setBackground(Color.LIGHT_GRAY);
        lblWeightLong.setOpaque(true);

        final JTextField txtWeightLong = new JTextField();
        txtWeightLong.setSize(90, 20);
        txtWeightLong.setLocation(100, 5);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(190, 20);
        btnCalculate.setLocation(200, 5);

        final JLabel lblResult = new JLabel(" Rectangle Weight&Long= ? area "
                + "= ? perimeter= ? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 100);
        lblResult.setBackground(Color.magenta);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 50);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtWeightLong.getText();
                    double weight = Double.parseDouble(strBase);
                    double Long = Double.parseDouble(strBase);
                    Rectangle rectangle = new Rectangle(weight, Long);
                    lblResult.setText(" Rectangle Weight&Long = "
                            + rectangle.getWeight()+ " area = "
                            + String.format("%.2f", rectangle.calArea())
                            + " perimeter = "
                            + String.format("%.2f", rectangle.calperimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error:Pleasr input"
                            + " number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWeightLong.setText("");
                    txtWeightLong.requestFocus();
                }
            }

        });

        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtWeightLong);
        frame.add(lblWeightLong);
        frame.setVisible(true);
    }    
}
