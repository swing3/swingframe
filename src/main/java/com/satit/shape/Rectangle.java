/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

/**
 *
 * @author Satit Wapeetao
 */
public class Rectangle extends Shape{
    private double weight;
    private double Long;
    public Rectangle(double weight,double Long) {
        super("Rectangle");
        this.weight=weight;
        this.Long=Long;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double Long) {
        this.Long = Long;
    }
    
    @Override
    public double calArea() {
        return weight*Long;
    }

    @Override
    public double calperimeter() {
        return 2*(weight*Long);
    }
    
}
