/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.*;


/**
 *
 * @author Satit Wapeetao
 */
public class SquareFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Square");
        frame.setSize(300,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblSide = new JLabel("side:",JLabel.TRAILING);
        lblSide.setSize(50,20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.LIGHT_GRAY);
        lblSide.setOpaque(true);
        
        final JTextField txtSide = new JTextField();
        txtSide.setSize(50,20);
        txtSide.setLocation(60, 5);
        
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(120, 5);
        
        
        final JLabel lblResult = new JLabel("Square side= ? area = ? perimeter= ? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300,50);
        lblResult.setBackground(Color.magenta);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 50);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strSide = txtSide.getText();
                double side = Double.parseDouble(strSide);
                Square square = new Square(side);
                lblResult.setText("Square side = "+square.getS()
                        +" area = "+String.format("%.2f", square.calArea())
                        +" perimeter = "+String.format("%.2f",square.calperimeter()));
            }catch(Exception ex){
                JOptionPane.showMessageDialog(frame,"Error:Pleasr input number",
                        "Error", JOptionPane.ERROR_MESSAGE);
                txtSide.setText("");
                txtSide.requestFocus();
            }           
            }    
            
        });
        
        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtSide);
        frame.add(lblSide);
        frame.setVisible(true);
    }
}
