/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Satit Wapeetao
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblHeightBase = new JLabel("Height&Base:", JLabel.TRAILING);
        lblHeightBase.setSize(90, 20);
        lblHeightBase.setLocation(5, 5);
        lblHeightBase.setBackground(Color.LIGHT_GRAY);
        lblHeightBase.setOpaque(true);

        final JTextField txtHeightBase = new JTextField();
        txtHeightBase.setSize(90, 20);
        txtHeightBase.setLocation(100, 5);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(190, 20);
        btnCalculate.setLocation(200, 5);

        final JLabel lblResult = new JLabel(" Triangle Height&Base= ? area "
                + "= ? perimeter= ? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 100);
        lblResult.setBackground(Color.magenta);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 50);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtHeightBase.getText();
                    double Base = Double.parseDouble(strBase);
                    double Height = Double.parseDouble(strBase);
                    Triangle triangle = new Triangle(Height, Base);
                    lblResult.setText(" Triangle Height&Base = "
                            + triangle.getHeight()+ " area = "
                            + String.format("%.2f", triangle.calArea())
                            + " perimeter = "
                            + String.format("%.2f", triangle.calperimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error:Pleasr input"
                            + " number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeightBase.setText("");
                    txtHeightBase.requestFocus();
                }
            }

        });

        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtHeightBase);
        frame.add(lblHeightBase);
        frame.setVisible(true);
    }
}
